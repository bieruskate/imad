import numpy as np
from sklearn import clone

from classifier_base import ClassifierBase
from mixins import MultiClassMixin


class AdaBoost(ClassifierBase, MultiClassMixin):
    def __init__(self, base_clf, n_cls=5, epsilon=.0001, bootstrap=True, max_samples=None):
        self.base_clf = base_clf
        self.n_cls = n_cls

        self.classifier_weights = []
        self.classifiers = []
        self.epsilon = epsilon

        self.bootstrap = bootstrap
        self.max_samples = max_samples

    def fit(self, X, y):
        super().fit(X, y)

        number_of_samples = X.shape[0]

        if not self.max_samples:
            self.max_samples = number_of_samples

        sample_weights = np.array([1 / number_of_samples] * number_of_samples)

        for _ in range(self.n_cls):
            clf = clone(self.base_clf)

            bootstrap_X, bootstrap_y = self._get_bootstrapped_dataset(sample_weights)
            clf.fit(bootstrap_X, bootstrap_y)
            self.classifiers.append(clf)

            y_pred = clf.predict(X)

            error_estimate = np.sum(sample_weights[y_pred != y])

            clf_weight = .5 * np.log((1 - error_estimate + self.epsilon) / (error_estimate + self.epsilon))

            self.classifier_weights.append(clf_weight)

            sample_weights[y_pred == y] *= (np.e ** (-clf_weight))
            sample_weights[y_pred != y] *= (np.e ** clf_weight)

            sample_weights = self.normalize_weights(sample_weights)

        return self

    def _get_bootstrapped_dataset(self, sample_weights):
        random_subset = np.random.choice(self.X_.shape[0], self.max_samples, replace=self.bootstrap, p=sample_weights)
        bootstrap_X = self.X_[random_subset]
        bootstrap_y = self.y_[random_subset]

        return bootstrap_X, bootstrap_y

    @staticmethod
    def normalize_weights(sample_weights):
        weights_sum = np.sum(sample_weights)
        return np.array([w / weights_sum for w in sample_weights])

    def predict(self, X):
        predictions = self._predict_by_all_classifiers(X)
        return self._combine_classifiers(predictions)

    def _combine_classifiers(self, predictions):
        supports = self.get_supports(predictions, self.classifier_weights)
        return [self.get_best_class_for_support_dict(support) for support in supports]

    def _predict_by_all_classifiers(self, X):
        return [clf.predict(X) for clf in self.classifiers]
