from adaboost import AdaBoost
from sklearn.tree import DecisionTreeClassifier
from sklearn import datasets
from sklearn.model_selection import cross_val_score

import numpy as np

if __name__ == '__main__':
    clf = AdaBoost(base_clf=DecisionTreeClassifier(), n_cls=50)

    wine_data = datasets.load_breast_cancer()

    X = wine_data.data
    y = wine_data.target

    print(np.mean(cross_val_score(clf, X, y, cv=5)))
