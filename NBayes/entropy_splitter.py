import numpy as np

class EntropySplitter:
    def __init__(self, gain_threshold=0, split_points=1):
        self.splits = []
        self.gain_threshold = gain_threshold
        self.split_points = split_points
        self._splits_already = 0

    def get_splits(self, x, y):
        x, y = self._to_numpy_arrays(x, y)

        splits_gains = self._get_entropy_gains_for_possible_splits(x, y)

        if splits_gains != {}:
            best_split = max(splits_gains, key=splits_gains.get)

            if splits_gains[best_split] > self.gain_threshold and self._splits_already < self.split_points:
                self._splits_already += 1
                self.splits.append(best_split)

                self.get_splits(x[x <= best_split], y[x <= best_split])
                self.get_splits(x[x > best_split], y[x > best_split])

        return sorted(self.splits)

    def _get_entropy_gains_for_possible_splits(self, x, y):
        set_entropy = self._calculate_entropy(y)

        possible_splits = [(a + b) / 2 for a, b in zip(x, x[1:])]

        splits_gains = {}
        for split in possible_splits:
            left_bin = y[x <= split]
            right_bin = y[x > split]

            gain = set_entropy - self._calculate_entropy_for_bins(left_bin, right_bin)
            splits_gains[split] = gain

        return splits_gains

    def _calculate_entropy_for_bins(self, left_bin, right_bin):
        left_bin_entropy = self._calculate_entropy(left_bin)
        right_bin_entropy = self._calculate_entropy(right_bin)

        bins_len = len(left_bin) + len(right_bin)
        left_probability = len(left_bin) / bins_len
        right_probability = len(right_bin) / bins_len
        
        return left_probability * left_bin_entropy + right_probability * right_bin_entropy

    @staticmethod
    def _calculate_entropy(series):
        _, counts = np.unique(series, return_counts=True)
        set_len = len(series)

        entropies = [- count / set_len * np.log2(count / set_len) for count in counts]
        return np.sum(entropies)

    @staticmethod
    def _to_numpy_arrays(x, y):
        return np.array(x), np.array(y)
